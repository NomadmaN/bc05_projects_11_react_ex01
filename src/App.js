import logo from './logo.svg';
import './App.css';
import BaiTapThucHanhLayout from './BaiTapLayoutComponent/BaiTapThucHanhLayout.jsx'

function App() {
  return (
    <div>
      <BaiTapThucHanhLayout></BaiTapThucHanhLayout>
    </div>
  );
}

export default App;
