import React, { Component } from 'react'
import Banner from './Banner.jsx'
import Item from './Item.jsx'

export default class Body extends Component {
  render() {
    return (
      <div>
        <Banner></Banner>
        <Item></Item>
      </div>
    )
  }
}
